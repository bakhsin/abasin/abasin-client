FROM node:14-alpine

# آرگومان‌ها
ARG USERNAME=node
ARG UID=999
ARG WORKDIR=/home/${USERNAME}/app

# ایجادیدن کاربر ناریشه
RUN deluser --remove-home node \
    && addgroup -S ${USERNAME} -g ${UID} \
    && adduser -S -G ${USERNAME} -u ${UID} ${USERNAME}
USER ${USERNAME}

# نیازمندی‌ها
ENV NODE_ENV=development \
    NPM_CONFIG_PREFIX=/home/${USERNAME}/.npm-global \
    PATH=$PATH:/home/${USERNAME}/.npm-global/bin
RUN npm install -g @vue/cli

# پوشه‌کاری
RUN mkdir -p ${WORKDIR}
WORKDIR ${WORKDIR}

# وابستگی‌ها
COPY ./package.json ./package-lock.json ./
RUN npm install

# رونشتن پروژه
COPY . ${WORKDIR}

# درگاه
EXPOSE 8080

# دستور آغارین
CMD [ "npm", "run", "serve -- --port 8080" ]
